"""
###############################################################################
ISLa - Intelligent Systems Lab
###############################################################################
MCTS - Monte Carlo Tree Search
Copyright 2022 © Alberto Castellini, Alessandro Farinelli, Federico Bianchi, Edoardo Zorzi
This file is part of MCTS.
MCTS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
MCTS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with MCTS.  If not, see <http://www.gnu.org/licenses/>.
Please, report suggestions/comments/bugs to
 alberto.castellini@univr.it, alessandro.farinelli@univr.it, federico.bianchi@univr.it, edoardo.zorzi@studenti.univr.it
"""

import numpy as np
import random

uniform = True


# Class of global parameters
class ExpParams():

    def __init__(self, n_actions, n_states, exp_cost, gamma, max_depth, transition, reward, final_states,
                state_node_type='uniform', fast_ucb=None, fast_ucb_limit=0):        
        
        # Number of possible actions
        self.n_actions = n_actions
        # Number of states
        self.n_states = n_states
        # Exploration constant
        self.exp_cost = exp_cost
        # Discount factor
        self.gamma = gamma
        # Max depth reachable by simulating
        self.max_depth = max_depth
        # Transition matrix
        self.T = transition
        # Sum of probabilities for each state-action
        # We use it to check if transition with MLE T is possibile (useful with MCTS-SPIBB)
        # *Maybe we can optimize the use of this variable saving memory*
        self.T_sum = np.zeros((n_states, n_actions))
        for x in range(n_states):
            for y in range(n_actions):
                self.T_sum[x, y] = np.sum(self.T[x][y])   
        # Reward matrix
        self.R = reward
        
        self.fast_ucb = fast_ucb
        self.fast_ucb_limit = fast_ucb_limit
        self.state_node_type = state_node_type
        if fast_ucb is not None:
            indices = np.indices((fast_ucb_limit, fast_ucb_limit))
            self.fast_ucb = np.log(indices[1])
            self.fast_ucb /= indices[0]
            self.fast_ucb = exp_cost * np.sqrt(self.fast_ucb)
            # [0][0] and [0][1] are wrong: we fix them
            self.fast_ucb[0][0] = float('+inf')
            self.fast_ucb[0][1] = float('+inf')
        # Terminal states
        self.terminals = final_states
        self.legal_actions = np.zeros((n_states, n_actions))
        for s in range(0, n_states):
            for a in range(0, n_actions):
                if np.sum(self.T[s][a]) > 1e-4 or s in self.terminals:
                    self.legal_actions[s][a] = 1
                else:
                    if fast_ucb is not None:
                        self.fast_ucb[s][a] = float('-inf')




exp_params = None



# Class of Action node
class ActionNode():
    """
    An action node, i.e. an instance representing an action in the tree (linked to a parent state node, so basically a state-action node)
    """
    def __init__(self, action, rollout=True, _print=False, _file=None, action_list=[]):
                
        self.action = action
        self.visited = []
        self.states = dict()
        self.n = 0
        self._value = 0
        self._total = 0
        self._rollout = rollout
        self._print = False
        self._open = True if _file is None else False
        self.file = _file  


    # The value of action node
    def value(self, uct=False):
        if self.n != 0:
            return self._total/self.n
        return 0


    # Simulates from the action (i.e. state-action node)
    def simulate(self, state, depth):
        
        self.n += 1
        
        # Check if the transition is null (possibly by using the transition matrix of MLE) 
        # or if the max depth has been reached
        # or a final state has been reached
        # --> return a reward
        if (exp_params.T_sum[state][self.action] <= 0 + 0.00001) or depth > exp_params.max_depth or state in exp_params.terminals:
            self._value += exp_params.R[state][self.action]

            return exp_params.R[state][self.action]

        # Select the next state
        s_prime = np.random.choice(range(0, exp_params.n_states), p=exp_params.T[state][self.action])
        
        if self._rollout:
            # Has not been visited before: rollout
            if s_prime not in self.visited:
                self.states[s_prime] = StateNodeNew(state=s_prime)

                delayed_reward = exp_params.gamma * self.states[s_prime].rollout(s_prime, depth + 1)            
            else:
                delayed_reward = exp_params.gamma * self.states[s_prime].simulate(depth + 1)

        else:
            # expand if not visited
            if s_prime not in self.visited:
                node = StateNodeNew(state=s_prime, _file=self.file)
                self.states[s_prime] = node


        self.visited.append(s_prime)

        # again, if we do not rollout we need to simulate again to get the delayed reward
        if not self._rollout:
                        
            delayed_reward = exp_params.gamma * self.states[s_prime].simulate(depth + 1)
            
        actual_reward = exp_params.R[state][self.action]

        reward = (actual_reward + delayed_reward)

        self._total += reward
        
        return reward    



# Class of State node A (StateNode is argument of StateNodeNew)
class StateNode():
    def __init__(self, state=0, initial_n=0, initial_val=0, root=False, _file=None):
               
        self.root = root
        self.file = _file
        self.state = state
        self.initial_n = initial_n
        self.initial_val = initial_val
        self.fast_ucb_limit = exp_params.fast_ucb_limit       
        # Number of visits
        self.n = 0
        # The value of node
        self.totalValue = 0
        self.actions = dict()
        # Create all the action nodes
        self.create_children_all()
        # Number of visits for each action nodes
        self.ns = [self.initial_n for i in range(0, exp_params.n_actions)]
        # Values in each action node
        self.values = [self.initial_val for i in range(0, exp_params.n_actions)]
        self._value = 0 # Total value
        self.type_node = exp_params.state_node_type      
        self.rewards = []


    # Start simulations from root node by simulating "n_sims" times
    def build_tree(self, n_sims):       
        for s in range(n_sims):           
            self.simulate(0)
        
        # Return the stats of root node
        return self.stats()


# Class of State node B
class StateNodeNew(StateNode):
    
    
    # Compute the stats of the root node: Q-values, number of visits, V
    def stats(self):
        return [a.value() for a in self.actions.values()], self.ns, self.value_of_node(), 


    # Create the action nodes
    def create_children_all(self):
        """Expand all actions"""
        for a in range(0, exp_params.n_actions):
            if self.root:
                file = open(f'Action={a}', 'a')
                newNode = ActionNode(a, _print=True, _file=file)
            else:
                newNode = ActionNode(a, _print=False, _file=self.file)
            self.actions[a] = newNode


    # Simulate from the action node
    def simulate(self, depth):
        """Simulate"""
        
        action = self.pick_action()
               
        # **Recursive call**  Simulate from the action node
        reward = self.actions[action].simulate(self.state, depth=depth)
            
        # Update statistics of action and state nodes
        self.update_node_stats(action, reward)
                
        return reward
    

    # Rollout function
    def rollout(self, state, depth):
        
        # Pick action
        action = self.pick_action(state=state)      
     
        # Check if the transition is null (possibly by using the transition matrix of MLE) 
        # or the max depth of simulation has been reached
        # or a final state has been reached
        # --> return a reward
        if (exp_params.T_sum[state][action] <= 0 + 0.00001) or depth > exp_params.max_depth or state in exp_params.terminals:
            return exp_params.R[state][action]
     
        actual_reward = exp_params.R[state][action]

        # **Recursive call** Rollout continues at depth + 1
        delayed_reward = exp_params.gamma * self.rollout(np.random.choice(range(0, exp_params.n_states), p=exp_params.T[state][action]), depth + 1)
        
        return actual_reward + delayed_reward


    # Application of UCT
    def pick_action_with_UCT(self):    
        # Select first randomly unvisited action nodes             
        if (0 in self.ns):
            indices = [i for i, x in enumerate(self.ns) if x == 0]
            probs = [1/len(indices)]*len(indices)
            
            return random.choices(population=indices, weights=probs)[0]  
        # Select action with UCT
        else:            
            updated_vals = [self.actions[a].value(uct=True) + exp_params.exp_cost*np.sqrt(np.log(self.n)/self.ns[a]) for a in range(exp_params.n_actions)]

        return np.argmax(updated_vals)
    

    # Action selection strategy
    def pick_action(self, state=None):
        in_rollout = True
        if state is None:
            in_rollout = False
            state = self.state            
        """Action selection strategy"""  
        # Use UCT          
        if not in_rollout:
            return self.pick_action_with_UCT()    
        # In rollout -> random choice
        else:               
            return np.random.choice(range(exp_params.n_actions))


    # Update the stats of node
    # n: the number of visit of state node
    # ns: the number of visit of action-node related to action received in input
    # totalValue: the value of state node
    def update_node_stats(self, action, reward, n=1):  
        self.n += n
        self.ns[action] += n
        self.totalValue += reward


    # Compute the value V of the root            
    def value_of_node(self):
        """Value as computed by the classic UCT strategy, not ours"""
        if self.n != 0:
            return self.totalValue / self.n
        return 0



class mcts():
    """
    Class for MCTS-SPIBB. Its purpose is to hold the root of the trees, which will 'start' from an instance of StateNodeNew
    """

    def __init__(self, gamma, nb_states, nb_actions, model, reward, n_sims, max_depth, exploration_costant, type_node, states_to_sim, final_states):

        self.nb_states = nb_states
        self.nb_actions = nb_actions
        self.final_states = final_states
        self.states_to_sim = states_to_sim
        self.nodes = dict()
        self.q_values = np.zeros((self.nb_states, self.nb_actions))
        self.touched = np.zeros((self.nb_states,))
        self.v = np.zeros((self.nb_states,))
        self.ns = np.zeros((self.nb_states, self.nb_actions))
        self.pi = np.zeros((self.nb_states, self.nb_actions))
        self.max_depth = max_depth
        self.gamma = gamma
        self.n_sims = n_sims
        self.P = model
        self.R = reward
        self.exploration_costant = exploration_costant
        self.type_node = type_node
    
    def computeTree(self, s):
        """
        Parameters: 
            s The state for which we want to compute the tree
        """
        # Global instance to hold the experiment parameters (save memory)
        global exp_params
        
        exp_params = ExpParams(self.nb_actions, self.nb_states, self.exploration_costant, self.gamma,
            self.max_depth, self.P, self.R, self.final_states, self.type_node)      
        
        # Root node
        root = StateNodeNew(state=s, root=True)
        
        # Return all info for the state (n_visits, total_return, value etc.) building the tree from the root
        return root.build_tree(self.n_sims)


    def fit(self, state_to_sim = None):

        # Because we can either pass it to the function or in the constructor
        if state_to_sim is not None:
            state_sims = state_to_sim
        else:
            state_sims = self.states_to_sim
        
        # Hold the results for each state
        results = dict()

        for s in state_sims:
            results[s] = self.computeTree(s)
            
        # Now results hold various statistics., We unfold them
        for s in state_sims:
            self.q_values[s] = results[s][0]
            self.ns[s] = results[s][1]
            self.v[s] = results[s][2]
            self.pi[s] = self.new_pi(s, np.array(results[s][0]))


    # Q must contain the tree Q values
    def new_pi(self, s, q):
        """Build the new pi from the Q-values"""        
        
        pi = self.pi[s].copy()
        pi[np.argmax(q)] = 1
        
        return pi


    @staticmethod
    def create_node_index(prev_index, state, action):
        if prev_index == '':
            comma_or_not = ''
        else:
            comma_or_not = ','
        if state == None:
            return f'{prev_index}{comma_or_not}{action}'
        return f'{prev_index}{comma_or_not}{state},{action}'
