#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
###############################################################################
ISLa - Intelligent Systems Lab
###############################################################################
MCTS - Monte Carlo Tree Search
Copyright 2022 © Alberto Castellini, Alessandro Farinelli, Federico Bianchi, Edoardo Zorzi
This file is part of MCTS.
MCTS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
MCTS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with MCTS.  If not, see <http://www.gnu.org/licenses/>.
Please, report suggestions/comments/bugs to
 alberto.castellini@univr.it, alessandro.farinelli@univr.it, federico.bianchi@univr.it, edoardo.zorzi@studenti.univr.it
"""

###############################################################################
# Static computation of MCTS that computes policy for a set a states
###############################################################################

import numpy as np
import mcts
from math import ceil, log

n_machines = 7
n_states = 2**n_machines
n_actions = n_machines + 1

# Matrices
transition_matrix = np.load ("matrices/Sysadmin_%s_T.npy" %n_machines)
reward_model = np.load ("matrices/Sysadmin_%s_R.npy" %n_machines)

# Discount factor
gamma = 0.85

# MCTS parameters
exp_constant = 5
tree_depth = ceil(log(0.01, gamma))
type_node = "uniform"
n_sim = [100]
final_states = [n_states]
initial_state = 0

# Uncomment this to simulate all the state space 
# states_to_sim = list(range(n_states))

# Simulate only a state, in this case the initial state s0
states_to_sim = [initial_state]

# Initialization
seed = 1
np.random.seed(seed) 
pib_MCTS = mcts.mcts(gamma, n_states, n_actions, transition_matrix, reward_model, n_sim[0], tree_depth, exp_constant, type_node, states_to_sim, final_states)

# Policy computation
pib_MCTS.fit()

# How to get the policy, Q-values, V, number of visits for each action node
mcts_policy = pib_MCTS.pi
mcts_Q = pib_MCTS.q_values
mcts_V = pib_MCTS.v
ns = pib_MCTS.ns


        



