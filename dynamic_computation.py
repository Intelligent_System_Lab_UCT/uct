#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
###############################################################################
ISLa - Intelligent Systems Lab
###############################################################################
MCTS - Monte Carlo Tree Search
Copyright 2022 © Alberto Castellini, Alessandro Farinelli, Federico Bianchi, Edoardo Zorzi
This file is part of MCTS.
MCTS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
MCTS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with MCTS.  If not, see <http://www.gnu.org/licenses/>.
Please, report suggestions/comments/bugs to
 alberto.castellini@univr.it, alessandro.farinelli@univr.it, federico.bianchi@univr.it, edoardo.zorzi@studenti.univr.it
"""

###############################################################################
# Dynamic computation of MCTS that computes policy dynamically by simulating steps in real environment
###############################################################################

import numpy as np
import mcts
import random
from math import ceil, log

n_machines = 7
n_states = 2**n_machines
n_actions = n_machines + 1

# Matrices
transition_matrix = np.load ("matrices/Sysadmin_%s_T.npy" %n_machines)
reward_model = np.load ("matrices/Sysadmin_%s_R.npy" %n_machines)

# Discount factor
gamma = 0.85

# MCTS parameters
exp_constant = 5
tree_depth = ceil(log(0.01, gamma))
type_node = "uniform"
n_sim = [100]
final_states = [n_states]
initial_state = 0

states_to_sim = [initial_state]

# Parameters of execution
runs = 1
steps = 30


# 1. Compute the policy for the initial state
seed = 1
np.random.seed(seed) 
# Initialization
pib_MCTS = mcts.mcts(gamma, n_states, n_actions, transition_matrix, reward_model, n_sim[0], tree_depth, exp_constant, type_node, states_to_sim, final_states)

# Policy computation
pib_MCTS.fit()

        
# 2. Start the runs
# List of states that the policy has been computed
mcts_state_list = []
for r in range(runs): 
    print("Run: %s" %r)

    # List of reward
    mcts_reward_list = []
    # List of selected actions
    mcts_action_list = []

    # At beginning of each run the initial state is 0
    state_t_mcts = initial_state

    # Add the state to list of states for which the policy has been already computed                   
    if(state_t_mcts not in mcts_state_list):
        mcts_state_list.append(state_t_mcts)    
          
    # 3. Select an action            
    action_i_mcts = random.choices(population = range(n_actions), weights = pib_MCTS.pi[state_t_mcts])[0]                                
    mcts_action_list.append(action_i_mcts)
    # 4. Receive a reward
    mcts_reward_list.append(reward_model[state_t_mcts][action_i_mcts])
            
    # 5. From the initial state move forward for "steps" steps                           
    for s in range(steps):
        print("** Step: %s" %s)
        
        # 6. Move to the next state                           
        state_t_mcts = random.choices(population=range(n_states), weights=transition_matrix[state_t_mcts][action_i_mcts])[0]

        # Check if for the next state the policy has not been computed                   
        if(state_t_mcts not in mcts_state_list):      
            # Add the next state to the list of states with policy
            mcts_state_list.append(state_t_mcts)            
            np.random.seed(seed)                    
            # 7. Compute the policy for this state  
            # Initialization
            states_to_sim = [state_t_mcts]
            pib_MCTS_new_state = mcts.mcts(gamma, n_states, n_actions, transition_matrix, reward_model, n_sim[0], tree_depth, exp_constant, type_node, states_to_sim, final_states)


            # Policy computation
            pib_MCTS_new_state.fit()
            # We add the new row of policy to the final policy
            pib_MCTS.pi[state_t_mcts] = pib_MCTS_new_state.pi[state_t_mcts]
                

        # 7. Select an action using the updated policy                                                                                                      
        action_i_mcts = random.choices(population = range(n_actions), weights = pib_MCTS.pi[state_t_mcts])[0]                                 
        mcts_action_list.append(action_i_mcts)
        # 8. Receive a reward
        mcts_reward_list.append(reward_model[state_t_mcts][action_i_mcts]*pow(gamma,s))   

        # If a final state is reached, stop the run
        if(state_t_mcts in final_states):
            break        
